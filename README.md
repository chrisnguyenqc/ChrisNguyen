- 👋 Hi, I’m Chris Nguyen
- 👀 I’m interested in Automation Testing
- 🌱 I’m currently a QC software
- 💞️ I’m looking to collaborate on my projects
- 📫 Contact me: Chrisnguyenqc@gmail.com / +84 0905 68 50 18

<!---
ChrisNguyenQC/ChrisNguyenQC is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
